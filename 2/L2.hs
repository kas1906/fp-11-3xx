
-- Названия функций: буквы,
-- строчные и заглавные, цифры, подчеркивание, '
-- Должно начинаться со строчной буквы (или _)
-- Арифметические операции - как обычно (+,-,*,скобки)
-- Деление - /, div, mod
fN4_me :: Integer -> Integer -> Integer
fN4_me arg1 arg2 = (arg1+arg2)*2

{-
1.0 / 2
0.5
*Main> div 1 2
0
*Main> mod 1 2
1
-}

-- Вызовы: имя функции, через пробел аргументы
-- В REPL набрать :reload (:r) -- файл перезагрузится
f2 x = 1 + fN4_me x x

{- REPL, types:
*Main> :t fN4_me
fN4_me :: Num a => a -> a -> a
*Main> :t f2
f2 :: Num a => a -> a
*Main> f2 "a"

<interactive>:9:1:
    No instance for (Num [Char]) arising from a use of ‘f2’
    In the expression: f2 "a"
    In an equation for ‘it’: it = f2 "a"
*Main> f2 (+1)

<interactive>:10:1:
    Non type-variable argument in the constraint: Num (a -> a)
    (Use FlexibleContexts to permit this)
    When checking that ‘it’ has the inferred type
      it :: forall a. (Num a, Num (a -> a)) => a -> a
*Main>
-}

-- В функцию можно передавать функцию
g f x = f (f x)

-- Каррирование
g2 = fN4_me 1
-- на самом деле:
g2'' x = fN4_me 1 x
-- или даже так:
g2' = \x -> fN4_me 1 x

g3 y = fN4_me y 1

-- Лямбда-функция:
g3' = \y -> fN4_me y 1
-- \арг арг ... -> тело

-- Рекурсия!
fact 0 = 1
fact n = n * fact (n-1)
-- fact 5
-- = 5 * fact 4
-- = 5 * 4 * fact 3
-- = 5 * 4 * 3 * fact 2
-- = 5 * 4 * 3 * 2 * fact 1
-- = 5 * 4 * 3 * 2 * 1 * fact 0
-- = 5 * 4 * 3 * 2 * 1 * 1
-- = 120

-- pattern matching
h 0 x = x
h 1 x = 2*x
h x 0 = x*3
h x 1 = x*4
h x y = y*h (x-1) 0
h x y = 5

{-
h_wrong x x = 1
h_wrong x y = 0

[1 of 1] Compiling Main             ( L2.hs, interpreted )

L2.hs:80:9:
    Conflicting definitions for ‘x’
    Bound at: L2.hs:80:9
              L2.hs:80:11
    In an equation for ‘h_wrong’
Failed, modules loaded: none.
Prelude> 
-}


-- pattern guards
h2 x y | x==y      = 1
       | otherwise = 0

h3 x | x == 1 = 132123
     | True   = 42

-- otherwise = False

h4 x | x==4 = x +
              x
              + x*x / (x+1)
     | x==5 = 7
     | x==12 = 123
h4 x = x

h6 x = x*h5

h5 = 7

h7 x = 1 + (if x == 5 then 4 else 3)

-- лучше в виде guards, но можно и так
h8 x = if x==1 || 4<x && x<10 || x/=12
       then 5
       else 3

abs1 :: Double -> Double
abs1 x | x>=0 = x
       | otherwise = -x

h9 x = x==1

-- === is infix, so (===)
(===) True True = True
(===) False False = True
-- _ - anything
(===) _ _ = False

-- Можно определять сразу в инфиксной форме
x =+= y | x==y = False
        | otherwise = True

-- Обычные функции можно использовать в инфиксной записи:

-- k1 x = mod x 2
k1 x = x `mod` 2

-- Инфиксные функции можно использовать в префиксной записи:
-- k2 x = x + 1
k2 x = (+) x 1

-- unit type
h10 :: Integer -> ()
h10 x = ()

-- Bool - булевы значения
-- Integer - неограниченные целые
-- Int - 64-битные целые
-- Double - вещественные

-- Модуль
abs' :: Double -> Double
abs' x | x>=0 = x
       | otherwise = -x


-- Сколько может быть воды в n бутылках по 1.5 л
fromBottle :: Int -> Double
fromBottle n = 1.5 * fromIntegral n

-- Сколько бутылок по 1.5 л нужно для хранения указанного кол-ва воды
toBottle :: Double -> Int
toBottle x = round $ x/1.5 + 0.5
          -- round ( x/1.5 + 0.5 )
-- f $ x = f x

-- Перевод из целого в числовой (у нас Double): fromIntegral 
-- Перевод из вещественного в целый: truncate, round


